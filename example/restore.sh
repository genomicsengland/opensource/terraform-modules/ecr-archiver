#!/usr/bin/env bash

../modules/vault/restore.py \
  --bucket 025403892336-ecr-archive \
  --source my-repo \
  --target restore-repo