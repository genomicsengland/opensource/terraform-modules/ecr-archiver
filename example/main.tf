data "aws_caller_identity" "current" {}

resource "aws_ecr_repository" "test" {
  name = "my-repo"
}

resource "aws_ecr_repository" "restore" {
  name = "restore-repo"
}

module "vault" {
  source = "../modules/vault"

  name = "${data.aws_caller_identity.current.account_id}-ECR-Archive"
  tags = {
    Application = "My Important Application"
    Team        = "App Team"
  }
}

module "archiver" {
  source = "../modules/archiver"

  name = "ECR-Archiver"

  repositories = [
    aws_ecr_repository.test.name
  ]

  archive_bucket_id = module.vault.bucket_id

  tags = {
    Application = "My Important Application"
    Team        = "App Team"
  }
}
