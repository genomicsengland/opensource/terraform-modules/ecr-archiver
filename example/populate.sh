#!/usr/bin/env bash

SOURCE_ACCOUNT_ID="877059142592"
SOURCE_REPOSITORY="docker"
SOURCE_PROFILE="core_bakery_readonly"

TARGET_ACCOUNT_ID="025403892336"
TARGET_REPOSITORY="my-repo"
TARGET_PROFILE="sandbox"

# log in to the source registry
aws ecr get-login-password --region eu-west-2 --profile $SOURCE_PROFILE | docker login --username AWS --password-stdin $SOURCE_ACCOUNT_ID.dkr.ecr.eu-west-2.amazonaws.com

# pull a list of images from the registry
aws ecr list-images --repository-name $SOURCE_REPOSITORY --profile $SOURCE_PROFILE | jq -r '.imageIds[].imageTag' > /tmp/images.txt

# retag them all for the new repository
for image in $(cat /tmp/images.txt); do
  docker pull $SOURCE_ACCOUNT_ID.dkr.ecr.eu-west-2.amazonaws.com/$SOURCE_REPOSITORY:$image
  docker tag $SOURCE_ACCOUNT_ID.dkr.ecr.eu-west-2.amazonaws.com/$SOURCE_REPOSITORY:$image $TARGET_ACCOUNT_ID.dkr.ecr.eu-west-2.amazonaws.com/$TARGET_REPOSITORY:$image
done

# log in to the target registry
aws ecr get-login-password --region eu-west-2 --profile $TARGET_PROFILE | docker login --username AWS --password-stdin $TARGET_ACCOUNT_ID.dkr.ecr.eu-west-2.amazonaws.com

# push all to the new repository
for image in $(cat /tmp/images.txt); do
  docker push $TARGET_ACCOUNT_ID.dkr.ecr.eu-west-2.amazonaws.com/$TARGET_REPOSITORY:$image
done

# delete local versions of the images
for image in $(cat /tmp/images.txt); do
  docker rmi $SOURCE_ACCOUNT_ID.dkr.ecr.eu-west-2.amazonaws.com/$SOURCE_REPOSITORY:$image
  docker rmi $TARGET_ACCOUNT_ID.dkr.ecr.eu-west-2.amazonaws.com/$TARGET_REPOSITORY:$image
done
