variable "name" {
  type        = string
  description = "Bucket name"
}

variable "tags" {
  type        = map(string)
  description = "Tags to apply to the bucket"
  default = {
    Name = "ECR Archive Vault Bucket"
  }
}

variable "archive_days" {
  type        = number
  description = "Number of days before moving to archive storage"
  default     = 1
}

variable "deep_archive_days" {
  type        = number
  description = "Number of days before moving to deep archive storage"
  default     = null
}

variable "bucket_policy" {
  type        = string
  description = "Bucket policy to apply to the bucket"
  default     = null
}

variable "kms_key_id" {
  type        = string
  description = "KMS key ID to use for encryption"
  default     = null
}
