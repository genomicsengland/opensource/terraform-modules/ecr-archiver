# Archive Vault

This module creates an S3 bucket correctly configured to store container archive files.

## Restoration Script

The module ensures a restore script is included in the bucket to make it easy to restore files in future.

The script is stored at the root of the bucket as `restore.py` and can be used as follows:

```bash
restore.py -b <bucket name> -s <repo dir> [-m <manifest name>] [-t <target repo>]
```

| parameter | description |
|-----------|-------------|
| -b | The name of the bucket where archives are stored |
| -s | The directory containing manifests - will be the name of the original repository archives were created from |
| -m | The name of the manifest file to restore.  If omitted all manifests will be restored |
| -t | The name of the target repository to restore to. If omitted will be the same repo as the source parameter |

## Usage

```hcl
data "aws_caller_identity" "current" {}

module "vault" {
  source = "<module url>"

  name = "${data.aws_caller_identity.current.account_id}-ECR-Archive"
  tags = {
    Application = "My Important Application"
    Team        = "App Team"
  }
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| name | The name of the S3 bucket to create. | `string` | n/a | yes |
| tags | A map of tags to apply to the S3 bucket. | `map(string)` | `{}` | no |
| archive_days | Number of days before moving to archive storage. | `number` | `1` | no |
| deep_archive_days | Number of days before moving to deep archive storage (`archive_days` + 90 if not specified) | `number` | `null` | no |
| bucket_policy | The policy to apply to the bucket. | `string` | `null` | no |
| kms_key_id | The KMS key to use for encryption. | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| bucket_id | The Id of the S3 bucket created. |
| bucket_arn | The ARN of the S3 bucket created. |
