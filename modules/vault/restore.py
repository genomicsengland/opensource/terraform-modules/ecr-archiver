#!/usr/bin/env python3

import argparse
import json
import math
from typing import Tuple

import boto3


def format_bytes(bytes_: int, decimals: int = 2) -> str:
    if bytes_ == 0:
        return '0 Bytes'
    k = 1024
    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    i = int(math.log(bytes_) / math.log(k))
    return f"{round(bytes_ / k ** i, decimals)} {sizes[i]}"


def process_layer(registry_id: str, source_repository_name: str, target_repository_name: str, layer_digest: str, bucket: str) -> None:
    # check to see if the layer already exists in the target repository
    availability = ecr.batch_check_layer_availability(
        registryId=registry_id,
        repositoryName=target_repository_name,
        layerDigests=[layer_digest],
    )

    if availability['layers'][0]['layerAvailability'] == 'AVAILABLE':
        print(f'Layer {layer_digest} already exists in target repository')
        return

    print(f'Downloading layer: {layer_digest}')
    object = s3.get_object(
        Bucket=bucket,
        Key=source_repository_name + '/layers/' + layer_digest
    )
    body = object['Body'].read()

    # initiate a layer upload to ecr
    layer = ecr.initiate_layer_upload(
        registryId=current_account_id,
        repositoryName=target_repository_name
    )

    print(f"Uploading layer: {layer_digest}, uploadId: {layer['uploadId']}, size: {format_bytes(len(body))}")

    # upload the layer to the target ecr repository in max 20MB chunks
    chunk_max = 20000000
    index = 0
    while index < len(body):
        print(f"chunk index: {int(index/chunk_max)}")

        size = min(len(body) - index, chunk_max)
        response = ecr.upload_layer_part(
            registryId=current_account_id,
            repositoryName=target_repository_name,
            uploadId=layer['uploadId'],
            partFirstByte=index,
            partLastByte=index + size - 1,
            layerPartBlob=body[index:index + size]
        )
        index += size

    # complete the layer upload
    print('Completing Upload')
    ecr.complete_layer_upload(
        registryId=current_account_id,
        repositoryName=target_repository_name,
        uploadId=layer['uploadId'],
        layerDigests=[layer_digest]
    )


def get_manifest(bucket: str, key: str) -> Tuple[dict, str]:
    object = s3.get_object(Bucket=bucket, Key=key)
    manifest = object['Body'].read()
    parsed = json.loads(manifest)
    return parsed, object['ContentType']


def upload_manifest(registry_id: str, repository_name: str, image_manifest: str, image_manifest_media_type: str, image_tag: str) -> None:
    # check to see if the image already exists in the target repository
    image = ecr.batch_get_image(
        registryId=registry_id,
        repositoryName=repository_name,
        imageIds=[{
            'imageTag': image_tag
        }]
    )

    if image['images']:
        print(f'Image {image_tag} already exists in {repository_name}')
    else:
        ecr.put_image(
            registryId=registry_id,
            repositoryName=repository_name,
            imageManifest=image_manifest,
            imageManifestMediaType=image_manifest_media_type,
            imageTag=image_tag
        )
        print(f'Image: {image_tag} uploaded to {repository_name} successfully')


def parse_args() -> argparse.Namespace:
    # parse the parameters passed to the script
    parser = argparse.ArgumentParser(
        prog='restore.py',
        description='Restore container(s) from an Archive.',
    )
    parser.add_argument('-b', '--bucket',   required=True,  default=None,
                        help='The bucket name where the manifest and layers are stored')
    parser.add_argument('-s', '--source',   required=True,  default=None,
                        help='The repo directory in the bucket')
    parser.add_argument('-m', '--manifest', required=False, default=None,
                        help='The manifest name (omit for all manifests in repo directory)')
    parser.add_argument('-t', '--target',   required=False, default=None,
                        help='The repository to restore to')
    args = parser.parse_args()

    if args.target is None:
        args.target = args.source
    return args


if __name__ == '__main__':
    args = parse_args()

    # create a boto3 clients
    s3 = boto3.client('s3')
    ecr = boto3.client('ecr')
    sts = boto3.client('sts')

    current_account_id = sts.get_caller_identity().get('Account')

    if args.manifest is None:
        # get a list of all the manifests in the source repository
        manifest_list = []
        paginator = s3.get_paginator('list_objects_v2')
        page_iterator = paginator.paginate(Bucket=args.bucket, Prefix=f"{args.source}/tag:")
        for page in page_iterator:
            for item in page['Contents']:
                manifest_list.append(item['Key'].replace(f"{args.source}/", ''))
    else:
        manifest_list = [args.manifest]

    # process each manifest
    for manifest_name in manifest_list:
        print(f'Processing manifest: {manifest_name}')
        parts = manifest_name.split(':')

        # try to read the s3 object
        try:
            manifest, ContentType = get_manifest(bucket=args.bucket, key=f"{args.source}/{manifest_name}")

            # process all the layers
            layers = manifest['layers'] + [manifest['config']]
            for layer in layers:
                process_layer(
                    registry_id=current_account_id,
                    source_repository_name=args.source,
                    target_repository_name=args.target,
                    layer_digest=layer['digest'],
                    bucket=args.bucket
                )

            # upload the manifest to the target repository
            print(f'Uploading manifest: {parts[2]}:{parts[3]}  tagged {parts[1]}')

            upload_manifest(
                registry_id=current_account_id,
                repository_name=args.target,
                image_manifest=json.dumps(manifest),
                image_manifest_media_type=ContentType,
                image_tag=parts[1]
            )
        except Exception as e:
            print(f"{e.__class__.__name__}: {e}")
            print(f'Failed to process manifest: {manifest_name}')
            raise
