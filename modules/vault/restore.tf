resource "aws_s3_object" "this" {
  bucket       = aws_s3_bucket.this.id
  key          = "restore.py"
  source       = "${path.module}/restore.py"
  etag         = filemd5("${path.module}/restore.py")
  content_type = "text/x-python"
}
