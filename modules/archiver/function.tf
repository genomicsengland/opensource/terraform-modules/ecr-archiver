data "archive_file" "archiver" {
  type        = "zip"
  output_path = "archiver-bundle.zip"
  source {
    content  = file("${path.module}/src/index.mjs")
    filename = "index.mjs"
  }
}

resource "aws_lambda_function" "archiver" {
  function_name    = var.name
  description      = "Archiver function to back up ECR images"
  filename         = data.archive_file.archiver.output_path
  source_code_hash = data.archive_file.archiver.output_base64sha256
  role             = aws_iam_role.archiver.arn
  handler          = "index.handler"
  runtime          = var.runtime
  architectures    = ["arm64"]
  timeout          = var.lambda_config["timeout"]
  memory_size      = var.lambda_config["memory_size"]
  ephemeral_storage {
    size = var.lambda_config["ephemeral_storage"]
  }

  environment {
    variables = {
      REPOSITORIES = join(",", var.repositories)
      BUCKETNAME   = var.archive_bucket_id
      IMAGE_FILTER = var.image_filter
    }
  }
  tracing_config {
    mode = "Active"
  }
}

#tfsec:ignore:aws-cloudwatch-log-group-customer-key
resource "aws_cloudwatch_log_group" "logs" {
  name              = "/aws/lambda/${aws_lambda_function.archiver.function_name}"
  retention_in_days = 7
}
