variable "name" {
  type        = string
  description = "Name for the ECR Archiver function"
  default     = "ECR-Archiver"
}

variable "repositories" {
  type        = list(string)
  description = "List of repositories to check (or empty list for all repositories in this account)"
  default     = []
}

variable "schedule" {
  type        = string
  description = "AWS Cron Schedule for running the function"
  default     = "cron(30 2 * * ? *)"
}

variable "boundary_policy_arn" {
  type        = string
  description = "ARN of the policy to use as a boundary for the IAM role"
  default     = null
}

variable "runtime" {
  type        = string
  description = "node runtime version"
  default     = "nodejs18.x"
}

variable "archive_bucket_id" {
  type        = string
  description = "Name of the S3 bucket to store the archives in"
}

variable "kms_key_id" {
  type        = string
  description = "KMS key to use for encryption"
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "Tags to apply to the resources"
  default     = {}
}

variable "image_filter" {
  type        = string
  description = "If set, only archive images whose tag matches given regex"
  default     = ""
}

variable "lambda_config" {
  description = "Configuration values for the Lambda function"
  type = map(any)
  default = {
    timeout           = 300
    memory_size       = 1024
    ephemeral_storage = 512
  }
}