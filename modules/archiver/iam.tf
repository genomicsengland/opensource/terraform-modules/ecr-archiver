resource "aws_iam_role" "archiver" {
  name               = var.name
  assume_role_policy = data.aws_iam_policy_document.archiver_assume.json
  inline_policy {
    name   = "archiver"
    policy = data.aws_iam_policy_document.archiver.json
  }
  permissions_boundary = var.boundary_policy_arn
}

data "aws_iam_policy_document" "archiver_assume" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}


data "aws_iam_policy_document" "archiver" {
  statement {
    sid = "logging"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = ["arn:aws:logs:*:*:*"]
  }

  statement {
    actions = [
      "ecr:BatchGetImage",
      "ecr:DescribeRepositories",
      "ecr:GetDownloadUrlForLayer",
      "ecr:ListImages",
    ]
    resources = ["*"]
  }

  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetObject",
      "s3:PutObject",
    ]
    resources = [
      "arn:aws:s3:::${var.archive_bucket_id}",
      "arn:aws:s3:::${var.archive_bucket_id}/*",
    ]
  }

  dynamic "statement" {
    for_each = var.kms_key_id != "" ? [var.kms_key_id] : []
    iterator = kms_key
    content {
      actions = [
        "kms:GenerateDataKey",
      ]
      resources = [kms_key.value]
    }
  }
}
