resource "aws_cloudwatch_event_rule" "archiver" {
  name                = var.name
  description         = "Run ECR archiver as per the schedule"
  schedule_expression = var.schedule
  state               = "ENABLED"
}

resource "aws_cloudwatch_event_target" "archiver" {
  rule      = aws_cloudwatch_event_rule.archiver.name
  target_id = "Trigger-${var.name}"
  arn       = aws_lambda_function.archiver.arn
}

resource "aws_lambda_permission" "archiver" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.archiver.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.archiver.arn
}
