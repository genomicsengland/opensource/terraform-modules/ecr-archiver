import {ECR} from '@aws-sdk/client-ecr'
import {S3} from '@aws-sdk/client-s3'
import {Upload} from '@aws-sdk/lib-storage'

const ecr = new ECR({region: process.env.AWS_REGION})
const s3 = new S3({region: process.env.AWS_REGION})

const formatBytes = (bytes, decimals) => {
  if (bytes === 0) {
    return '0 Bytes'
  }
  let k = 1024,
    dm = decimals || 2,
    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
    i = Math.floor(Math.log(bytes) / Math.log(k))
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]
}

/**
 * Check if an object exists in S3
 * @param {string} bucketName - The name of the bucket to check
 * @param {string} key - The key of the object to check
 * @param {number} expectedSize - The size of the object
 * @returns {boolean} - True if the object exists and has the same size, false otherwise
 */
const objectExists = async (bucketName, key, expectedSize = -1) => {
  try {
    const result = await s3.headObject({
      Bucket: bucketName,
      Key: key
    })

    if (result.ContentLength === expectedSize) {
      return true
    }
  } catch (err) {
    if (err.name === 'NotFound') {
      return false
    } else {
      console.log(err)
    }
  }

  return false
}

/**
 * Get a list of images in a repository
 * @param {string} repositoryName
 * @param {string} filter - only return imageIds whose tag match filter
 * @returns {Promise[string]} - An array of imageIds
 */
const getImageList = async (repositoryName, filter) => {
  console.log(`Getting image list for ${repositoryName}`)
  const params = {
    repositoryName,
    filter: {
      tagStatus: filter === "" ? "ANY" : "TAGGED"
    }
  }

  const images = []

  const filter_re = new RegExp(filter);
  do {
    const result = await ecr.listImages(params)
    images.push(...result.imageIds.filter(x => (x.imageTag || "").match(filter_re)))
    params.nextToken = result.nextToken
  } while(params.nextToken)

  console.log(`Found ${images.length} images in ${repositoryName}`)
  return images
}

const getImageDetails = async (repositoryName, image) => {
  console.log(`Getting image details for ${image.imageTag} in ${repositoryName}`)
  const params = {
    repositoryName,
    imageIds: [image]
  }

  const result = await ecr.batchGetImage(params)
  if (result.failures.length !== 0) {
    console.log(`Error getting image details for ${image.imageTag} in ${repositoryName}`)
    for (const failure in result.failures) {
      console.log(failure.failureReason)
    }
    return undefined
  }
  if (result.images.length !== 1) {
    console.log(`Expected 1 image but got ${result.images.length}; skipping ${image.imageTag}`)
    return undefined
  }

  return result.images[0]
}

const writeManifests = async (repositoryName, imageDetail) => {
  console.log(`Writing manifest for ${imageDetail.imageId.imageTag} in ${repositoryName}`)
  // write the manifest to the S3 bucket at <root>/<repositoryName>/tag:<imageTag><imageDigest> using the mimetype specified in the imageManifest.config.mediaType property
  const Key = `${repositoryName}/tag:${imageDetail.imageId.imageTag}:${imageDetail.imageId.imageDigest}`

  if (await objectExists(process.env.BUCKETNAME, Key, imageDetail.imageManifest.length)) {
    console.log(`Manifest: Skipping ${Key} as it already exists`)
    return
  }

  await s3.putObject({
    Bucket: process.env.BUCKETNAME,
    Key,
    Body: imageDetail.imageManifest,
    ContentType: imageDetail.imageManifestMediaType,
  })
  console.log(`Manifest: Wrote ${Key} to S3`)
}

const getLayerList = async (repositoryName, imageDetail) => {
  const layerList = []

  const manifest = JSON.parse(imageDetail.imageManifest)

  // write all the layer details to the layerList
  layerList.push(manifest.layers.map(layer => ({
    layerDigest: layer.digest,
    size: layer.size,
    mediaType: layer.mediaType
  })))

  // write the primary layer to the layer list
  layerList.push({
    layerDigest: manifest.config.digest,
    size: manifest.config.size,
    mediaType: manifest.config.mediaType
  })

  return layerList.flat()
}

const writeLayers = async (repositoryName, layerList) => {
  let errors = false
  for (const layer of layerList) {
    const Key = `${repositoryName}/layers/${layer.layerDigest}`

    if (await objectExists(process.env.BUCKETNAME, Key, layer.size)) {
      console.log(`Layer: Skipping ${Key} as it already exists`)
      continue
    }

    const url = await ecr.getDownloadUrlForLayer({
      repositoryName,
      layerDigest: layer.layerDigest
    })

    console.log(`Layer: Streaming ${Key} (${formatBytes(layer.size)})`)

    try {
      const startTime = performance.now()

      // start the layer download and get a read stream to pass to the upload
      const readStream = (await fetch(url.downloadUrl)).body

      const upload = new Upload({
        client: s3,
        params: {
          Bucket: process.env.BUCKETNAME,
          Key,
          ContentType: layer.mediaType,
          Body: readStream
        },
        // FIXME: upload breaks with multipart
        // 10 GB is the maximum size of a Docker image layer
        partSize: 1024 * 1024 * 1024 * 10, // default is 5MB
      })

      upload.on("httpUploadProgress", progress => { console.log(progress); });
      await upload.done()

      const endTime = performance.now()

      console.log(`Layer: upload complete ${formatBytes(layer.size)} (${formatBytes(layer.size / ((endTime - startTime) / 1000))}/s)`)
    } catch (err) {
      console.log(err)
      errors = true
    }
  }
  return !errors
}

const archiveRepository = async (repositoryName) => {
  console.log(`Archiving ${repositoryName}`)

  // get a list of all images in the repository
  const images = await getImageList(repositoryName, process.env.IMAGE_FILTER)

  // get the full details for each image
  for (const image of images) {
    const imageDetail = await getImageDetails(repositoryName, image)
    if (imageDetail) {
      const layerList = await getLayerList(repositoryName, imageDetail)
      if (await writeLayers(repositoryName, layerList)) {
        await writeManifests(repositoryName, imageDetail)
      } else {
        console.log(`Archiving ${image.imageTag} failed`)
      }
    }
  }
}

const getRepositories = async () => {
  let repositories = []
  if (process.env.REPOSITORIES && process.env.REPOSITORIES !== '') {
    repositories = process.env.REPOSITORIES.split(',')
  } else {
    const params = {}

    do {
      const result = await ecr.describeRepositories(params)
      repositories = repositories.concat(result.repositories.map(repo => repo.repositoryName))
      params.nextToken = result.nextToken
    } while(params.nextToken)
  }
  return repositories
}

export const handler = async () => {
  const repositories = await getRepositories()
  console.log('ECR Repositories to be archived: ', repositories)

  for (const repositoryName of repositories) {
    await archiveRepository(repositoryName)
  }

  return true
}