import { handler } from './index.mjs'

const local = async () => {
  const event = {}
  process.env.REPOSITORIES = 'my-repo'
  process.env.BUCKETNAME = '025403892336-ecr-archive'
  await handler(event)
}

local()