# ECR-Archiver

This module scans all containers in ECR repositories and archives them to S3.

## Usage

```hcl
data "aws_caller_identity" "current" {}

module "archiver" {
  source = "../modules/archiver"

  name = "${data.aws_caller_identity.current.account_id}-ECR-Archive"
  tags = {
    Application = "My Important Application"
    Team        = "App Team"
  }
}
```

## Inputs

| Name                  | Description                                                                   | Type         | Default              | Required |
|-----------------------|-------------------------------------------------------------------------------|--------------|----------------------|:--------:|
| `name`                | Name for the ECR Archiver function                                            | string       | "ECR-Archiver"       |    no    |
| `repositories`        | List of repositories to check (or blank for all repositories in this account) | list(string) | []                   |    no    |
| `schedule`            | AWS Cron Schedule for running the function                                    | string       | "cron(30 2 * * ? *)" |    no    |
| `boundary_policy_arn` | ARN of the policy to use as a boundary for the IAM role                       | string       | null                 |   yes    |
| `runtime`             | Node runtime version                                                          | string       | "nodejs18.x"         |    no    |
| `archive_bucket_id`   | Name of the S3 bucket to store the archives in                                | string       | null                 |   yes    |
| `image_filter`        | If set, only archive tagged images whose tag matches given regex              | string       | ""                   |    no    |
| `tags`                | Tags to apply to the resources                                                | map(string)  | {}                   |    no    |
