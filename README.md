# ECR-Archiver

This module contains two submodules that can be used to archive ECR repositories.

## Submodules

- [vault](modules/vault/README.md): Handles the creations of archive storage
- [archiver](modules/archiver/README.md): Handles archiving of whole ECR repositories
